#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <vector>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <bitset>
#include <iostream>


{

}

int main()
{
 
    CvCapture* capture =cvCreateCameraCapture(-1);
    if (!capture){
        printf("Error. Cannot capture.");
    }
    else{
        IplImage* frame = cvQueryFrame(capture);
        if(!frame){
            printf("Error. Cannot get the frame.");       
        }

        std::vector<int> params;
        params.push_back(CV_IMWRITE_JPEG_QUALITY);
		params.push_back(20);
		std::vector<unsigned char> buf;

		cv::Mat mBefore = cv::cvarrToMat(frame);
		cv::imencode(".jpeg", mBefore, buf, params);
		cv::Mat matFinal=cv::imdecode(buf,-1);
        cvReleaseCapture(&capture);

    }
    return 0;
}