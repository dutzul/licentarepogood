public class Sample1 {
   // Declare a native method sayHello() that receives nothing and returns void
   private native byte[] returnString();
 
   // Test Driver
   public static void main(String[] args) {
      System.load("/home/dutzul/testJNI/JNITEST/libhello.so");
      Sample1 caller=new Sample1();
      byte[] result=caller.returnString();  // invoke the native method
      for(int i=0;i<result.length;++i){
         System.out.println((int)result[i]);
      }
   }
}
