import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class JavaShowAndReceive {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket serverSocket = new ServerSocket(3005);
        Socket sock = serverSocket.accept();
        InputStream stream = sock.getInputStream();
        byte[] buffer= new byte[20000];

        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel label=new JLabel();

        int delay = 30;
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt){
                System.out.println("PULA");
                try {
                    int count = stream.read(buffer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                InputStream in = new ByteArrayInputStream(buffer);
                try {
                    BufferedImage img=ImageIO.read(in);
                    label.setIcon(new ImageIcon(img));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                f.getContentPane().add(label);
                f.setSize(600, 400);
                f.setLocation(200, 200);
                f.setVisible(true);
            }
        };
        new Timer(delay, taskPerformer).start();
        Thread.sleep(100000);
    }
}
