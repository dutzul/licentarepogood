import sun.plugin.util.UIUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
public class Receiver{

    private native byte[] returnString();
    static  BufferedImage myImage= new BufferedImage(100, 50, BufferedImage.TYPE_INT_ARGB);
    static Receiver caller=null;
    static Socket sock;
    static OutputStream stream ;

    private static class myRunnable implements Runnable{
        @Override
        public void run() {
            while (true) {
                byte[] buffer = null;
                buffer = caller.returnString();
                try {
                    stream.write(buffer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    public static void main(String[] args) throws Exception {
        System.load("/home/dutzul/testJNI/FolderComun/C++/libhello.so");
        caller= new Receiver();
        sock=new Socket("localhost",3005);
        stream = sock.getOutputStream();
        Thread thread = new Thread(new myRunnable());
        thread.start();
    }
}