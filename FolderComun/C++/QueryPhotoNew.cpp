#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <vector>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <bitset>
#include <iostream>
#include "Receiver.h"
#include <jni.h>

//g++ -shared -fPIC QueryPhotoNew.cpp -lopencv_core -lopencv_highgui -I /usr/lib/jvm/java-8-oracle/include -o libhello.so


JNIEXPORT jbyteArray JNICALL Java_Receiver_returnString
  (JNIEnv * env, jobject){

    static CvCapture* capture=cvCreateCameraCapture(-1);

    if (!capture){
        printf("Error. Cannot capture.");
    }
    else{
        IplImage* frame = cvQueryFrame(capture);
        if(!frame){
            printf("Error. Cannot get the frame.");
        }

        std::vector<int> params;
        params.push_back(CV_IMWRITE_JPEG_QUALITY);
        params.push_back(45);
        std::vector<unsigned char> buf;
        cv::imencode(".jpeg", cv::cvarrToMat(frame), buf, params);


        jbyteArray __ba = env->NewByteArray(buf.size());
        unsigned char * __c_ptr = buf.data();
        env->SetByteArrayRegion (__ba, 0, buf.size(), reinterpret_cast<jbyte*>(__c_ptr));
        //  cvWaitKey(30);

      //  if (okz==5)
      //      cvReleaseCapture(&capture);
            std::cout<<"PULA"<<std::endl;
        return __ba;
    }
}

int main()
{

    return 0;
}