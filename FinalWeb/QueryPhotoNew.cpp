#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <vector>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <bitset>
#include <iostream>

int main()
{
    cvNamedWindow("Window", CV_WINDOW_AUTOSIZE);
    CvCapture* capture =cvCreateCameraCapture(-1);
    if (!capture){
        printf("Error. Cannot capture.");
    }
    else{
        cvNamedWindow("Window", CV_WINDOW_AUTOSIZE);


        IplImage* frame = cvQueryFrame(capture);
        if(!frame){
            printf("Error. Cannot get the frame.");       
        }

        std::vector<int> params;
        params.push_back(CV_IMWRITE_JPEG_QUALITY);
		params.push_back(20);
		std::vector<unsigned char> buf;

		cv::Mat mBefore = cv::cvarrToMat(frame);
		cv::imencode(".jpeg", mBefore, buf, params);
		cv::Mat matFinal=cv::imdecode(buf,-1);

        unsigned int size_buf=buf.size();
        std::cout<<size_buf<<'\n';

		mkfifo("/tmp/myPipe", 0777);
		int fd=open("/tmp/myPipe",O_WRONLY,0777);
        
        write(fd,&size_buf,4);
	
    	for(int i=0;i<buf.size();++i){
			write(fd,&buf[i],1);
		}
		close(fd);

        //cvShowImage("Window",frame);
       // IplImage* image2=cvCloneImage(&(IplImage)matFinal);
       // cvSaveImage("wow.jpeg",image2);
        cvWaitKey(30);
        
        cvReleaseCapture(&capture);
        cvDestroyWindow("Window");
    }
    return 0;
}